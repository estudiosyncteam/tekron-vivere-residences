<?php include_once('header.php'); ?>

  <header id="header" class="pos-relative page-home">
    <div class="wrap flt-center floatfix">
      <?php include_once('inc/nav-menu.php'); ?>

      <div class="header-txt">
        <h1 class="tt uppercase color-1-1 fw-exlight">Tudo o que você precisa<br> pagando menos</h1>
        <p class="pp">Realize o sonho de ter seu primeiro imóvel com comodidade e conforto.</p>

        <a class="btn uppercase mf-popup" href="#form-leads-mp2">Fale com nossos consultores</a>
      </div>

      <article id="ellipse" class="icon icon-ellipse color-fff">
        <h1 class="tt-2">Apartamentos a partir de</h1>
        <p class="pp-2 uppercase">
          <span class="span-moeda flt-left">R$</span>
          <span class="span-preco flt-left">169</span><br>
          <span class="span-mil flt-left">mil</span></p>
      </article>
    </div>

    <a id="btn-next" href="#section-1" aria-hidden="true"><i class="icon icon-next"></i></a>
  </header>

  <?php include_once('inc/btn-show-mp.php'); ?>

  <main>
    <section id="section-1" class="wrap flt-center">
      <h1 class="tt-3 uppercase">Seja bem-vindo a uma nova forma de viver! Conheça o <span class="color-1-1">Vivere Residences</span>,<br>
      um condomínio fechado com área de lazer exatamente como você sempre sonhou.<br>
      Tudo que você precisa para morar bem localizado!
      </h1>
      <p class="pp">
        Nossos condomínios são privativos que contam com toda infraestrutura para que você e sua família tenham tudo em um só lugar: piscina,<br>
        churrasqueira, espaço gourmet e muito mais. Temos um dos melhores preços da região e condições imperdíveis para<br>
        a compra do seu imóvel.
      </p>
    </section>

    <section id="section-2">
      <div class="wrap flt-center content">
        <!--<h1 class="tt-3 uppercase">
          Nossas dependências foram planejadas<br>
          para dar a melhor experiência<br>
          de moradia PARA você
        </h1>-->
        <h1 class="tt-3 uppercase">
        NOSSAS DEPENDÊNCIAS FORAM PLANEJADAS</br>
        PARA DAR A MELHOR EXPERIÊNCIA</br>
        DE MORADIA PARA VOCÊ
        </h1>

        <ul class="list-topics">
          <li>2 blocos independentes;</li>
          <li>Térreo mais quatro andares;</li>
          <li>2 quartos;</li>
          <li>Hall privativo com um vizinho por porta;</li>
          <li>Estacionamento.</li>
        </ul>

        <a class="btn uppercase" href="planta.php">Saiba mais</a>
      </div>
    </section>

    <section id="section-3" class="wrap flt-center">
      <h1 class="tt-3 uppercase">
        Garanta sua qualidade de vida e aproveite cada ambiente de lazer que o Vivere Residences tem para oferecer.
      </h1>
      <ul class="list-figure color-1-1 fw-bold uppercase">
        <li>
          <figure class="fig-mask">
            <img src="img/ambiente-01.jpg" alt="ambiente">
          </figure>
          <p>Academia</p>
        </li><li>
          <figure class="fig-mask">
            <img src="img/ambiente-02.jpg" alt="ambiente">
          </figure>
          <p>Espaço Gourmet</p>
        </li><li>
          <figure class="fig-mask">
            <img src="img/ambiente-03.jpg" alt="ambiente">
          </figure>
          <p>Hortinha</p>
        </li><li>
          <figure class="fig-mask">
            <img src="img/ambiente-04.jpg" alt="ambiente">
          </figure>
          <p>Bicicletário e muito mais.</p>
        </li>
      </ul>

      <a class="btn uppercase mf-popup" href="#form-leads-mp4">Baixe nosso book</a>
    </section>

    <section id="section-4">
      <figure class="fig-sec-4 floatfix">
        <img class="flt-left" src="img/bg-espaco.jpg" alt="espaço">
      </figure>
    </section>

    <section id="section-5">
      <div class="wrap flt-center content">
        <h1 class="tt-3 uppercase">
          Localizado no Grande Méier, uma região consolidada, no Vivere Residences<br>
          você contará com a infraestrutura completa de comércio, serviços,<br>
          escolas, bancos e hospitais.
        </h1>
        <h2 class="subtt uppercase color-1-1">para você e toda sua família.</h2>
        <ul class="list-topics">
          <li>Rede Economia</li>
          <li>Colégio Pedro II</li>
          <li>Imperador</li>
          <li>Norte Shopping</li>
          <li>E muito mais!</li>
        </ul>

        <a class="btn uppercase" href="localizacao.php">Saiba mais</a>
      </div>
    </section>

    <section id="section-6">
      <div class="wrap flt-center content pos-relative">
        <figure class="figure">
          <img src="img/viver-com-mais.png" alt="viver-com-mais">
        </figure>
        <h1 class="tt-4 color-1-2 txt-center">Conheça nosso projeto<br> de relacionamento</h1>
        <a class="btn uppercase mf-popup" href="#form-leads-mp2">Saiba mais</a>
        <figure class="graf-familia xs-hidden">
          <img src="img/grafico-familia.png" alt="Família">
        </figure>
      </div>
    </section>
  </main>

<?php include_once('footer.php'); ?>
