<?php
//require_once 'wp-load.php';
/*
Força download do PDF
*/
header("Content-disposition: attachment; filename=book-vivere-residences.pdf");
header("Content-type: application/pdf");
header("Content-type: application/force-download");
function AbreSite ( $url ) {
   $site_url = $url;
   $ch = curl_init();
   $timeout = 5;
   curl_setopt ($ch, CURLOPT_URL, $site_url);
   curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
   ob_start();
   curl_exec($ch);
   curl_close($ch);
   $file_contents = ob_get_contents();
   ob_end_clean();
   return $file_contents;
 }
$site = $_SERVER['SERVER_NAME'].'/pdf/book-vivere-residences.pdf';
$conteudo = Abresite( $site );
echo $conteudo;
exit();
