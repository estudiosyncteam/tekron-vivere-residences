<?php
define('AMBIENTE', $_SERVER['APPLICATION_ENV']);

function sanitize_input($data) {
	$data = trim($data);
	$data = strip_tags($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data, ENT_QUOTES, "UTF-8");
	$data = str_replace('  ', ' ', $data);
	return $data;
}

if (!empty($_POST)) {
    $error = array();
    $body = '';
    $obrigatorios = array();
    if(isset($_POST['obrigatorios'])){
        $obrigatorios = json_decode(stripslashes($_POST['obrigatorios']));
    }

    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    //$nome_social = sanitize_input(preg_replace('/[^ \w]+/u', '',$_POST['nome_social']));
    if(isset($_POST['nome'])){
        $nome = sanitize_input($_POST['nome']);
        if(empty($nome)){
            $error['nome'] = 'Não utilize caracteres especiais exceto ponto(.), traço(-) e sublinhado(_)';
        }else{
            $body .= '<strong>Nome:</strong> '.$nome.' <br />';
        }
    }

		if(isset($_POST['email'])){
			$email = sanitize_input($_POST['email']);
	    if(!preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(.[[:lower:]]{2,3})(.[[:lower:]]{2})?$/", $email)) {
	        $error['email'] = 'Digite um e-mail válido';
	    }else{
	        $body .= '<strong>E-mail:</strong> '.$email.' <br />';
	    }
		}

    if(isset($_POST['telefone'])){
        $telefone = sanitize_input($_POST['telefone']);
        if(empty($telefone)){
            $error['telefone'] = 'Digite um número de telefone válido';
        }else{
            $body .= '<strong>Telefone:</strong> '.$telefone.' <br />';
        }
    }

		$local = '';
    if(isset($_POST['local'])){
        $local = sanitize_input($_POST['local']);
        if(empty($local)) {
            $error['local'] = 'Digite uma local';
        }else{
            $body .= '<strong>local:</strong> '.$local.'<br/>';
        }
    }

		$mensagem = '';
    if(isset($_POST['mensagem'])){
        $mensagem = sanitize_input($_POST['mensagem']);
        if(empty($mensagem)) {
            $error['mensagem'] = 'Digite uma mensagem';
        }else{
            $body .= '<strong>Mensagem:</strong> <p>'.$mensagem.'</p>';
        }
    }

		$origem = '';
    if(isset($_POST['origem'])){
        $origem = sanitize_input($_POST['origem']);
        if(empty($origem)) {
            $error['origem'] = 'Digite uma origem';
        }else{
            $body .= '<strong>Origem:</strong> '.$origem.'</br>';
        }
    }

		$email_mkt = '';
    if(isset($_POST['email_mkt'])){
      $email_mkt = sanitize_input($_POST['email_mkt']);
    }

		/*$tel_origem = "";
		if(isset($_POST['tel_origem'])){
			$tel_origem = !empty(sanitize_input($_POST['tel_origem'])) ? $_POST['tel_origem'] : $tel_origem;
		}*/

		$assunto = 'Contato';
		if(isset($_POST['assunto'])){
			$assunto = !empty(sanitize_input($_POST['assunto'])) ? $_POST['assunto'] : $assunto;
		}

    if(empty($error)) {
				
				$host = "vivereresidenc.mysql.dbaas.com.br";
				$userName = "vivereresidenc";
				$password = "T8x2e4f5";
				$dbName = "vivereresidenc";

       	// Create database connection
       	$conn = new mysqli($host, $userName, $password, $dbName);
				mysqli_set_charset($conn,"utf8");
       	$sql="INSERT INTO leads (nome, email, telefone, mensagem, local, formulario, user_agent) VALUES ('".$nome."','".$email."', '".$telefone."', '".$mensagem."', '".$local."', '".$assunto."', '".$user_agent."')";


       	if(!$result = $conn->query($sql)){
           	die('There was an error running the query [' . $conn->error . ']');
       	}

        // Envia email depois que grava
        require 'lib/phpmailer/PHPMailerAutoload.php';
        $mail = new PHPMailer;

        $mail->IsSMTP(); // enable SMTP

        if(AMBIENTE == 'dev'){
            $usuario = 'cliente@estudiosync.com.br';
            $usuarioFrom = 'cliente@estudiosync.com.br';
            $senha = 't8x2e4f5';
            $recipients = array(
							'filipe.angelo@estudiosync.com.br' => 'Comercial',
						);
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 465; // or 587
        }else{
            $usuario = 'cliente@estudiosync.com.br'; // From = Formulário Site
            $usuarioFrom = 'cliente@estudiosync.com.br';
            $senha = 't8x2e4f5';
            $recipients = array(
              'vendas@vivereresidences.com.br' => 'Comercial'
            );
						$mail->SMTPSecure = 'ssl'; // ssl or tls
						$mail->Host = "smtp.gmail.com"; //contato@prophylaxis.com.br
            $mail->Port = 465; // 587 or 465
        }

				$mail->CharSet = "UTF-8";
        //$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        $mail->IsHTML(true);
        $mail->Username = $usuario;
        $mail->Password = $senha;
        $mail->SetFrom( $usuarioFrom, 'Vivence Residences');
        $mail->Subject = "Interesse";
        $mail->Body = $body;
        foreach($recipients as $email => $name){
           $mail->AddAddress($email, $name);
        }


        if($mail->Send()){
					$retorno = array(
						'mensagem'=>'Obrigado pelo contato!<br>Nossos especialistas entrarão em contato no horário comercial.'
					);
					if($assunto == 'whatsapp'){
						$retorno['mensagem'] = 'Obrigado! Você será redirecionado apra o atendimento via Whatsapp.';
						$retorno['whatsapp'] = array('text'=>urlencode('Olá! Poderia me ajudar?'),'tel'=>'5521974755668');
					}
					if($assunto == 'download'){
						$retorno['mensagem'] = 'Obrigado! Download realizado com sucesso.';
						$retorno['download'] = 'download';
					}
					echo json_encode(array('sucesso'=>$retorno));
			}else{
					echo json_encode(array('erroEnvio'=>'Ocorreu um erro no envio do e-mail'));
			}
    }else{
        echo json_encode($error);
    }
}
die();
