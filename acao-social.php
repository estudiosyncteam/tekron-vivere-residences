<?php include_once('header.php'); ?>

  <header id="header" class="pos-relative page-acao-social">
    <div class="wrap flt-center floatfix">
      <?php include_once('inc/nav-menu.php'); ?>

      <div class="header-txt">
        <h1 class="tt uppercase color-1-1 fw-exlight">Ação Social</h1>
        <p class="pp">Investimos em ações sociais privadas que são desenvolvidas<br>
        por nós com o objetivo de melhorar a qualidade de vida das<br>
        comunidades vizinhas aos nossos empreendimentos.
        </p>
        <p class="pp">O engajamento dessas ações é criado por meio do diálogo contínuo e transparente.<br>
        Investimos também no Relacionamento Comunitário, reafirmando a nossa crença, de que novas<br>
        perspectivas de futuro para todos só é possível aliando o desenvolvimento econômico às ações de<br>
        impacto social e ambiental.
        </p>
      </div>

    </div>
  </header>

  <?php include_once('inc/btn-show-mp.php'); ?>

  <main>
    <section id="page-acao-social-1">
      <figure class="fig-acao-social floatfix">
        <img class="flt-left" src="img/acao-social/sec-acao-social-1.jpg" alt="acao-social">
      </figure>
    </section>
  </main>

<?php include_once('footer.php'); ?>
