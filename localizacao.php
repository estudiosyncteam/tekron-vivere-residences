<?php include_once('header.php'); ?>

  <header id="header" class="page-localizacao">
    <div class="wrap flt-center floatfix pos-relative">
      <?php include_once('inc/nav-menu.php'); ?>

      <div class="header-txt">
        <h1 class="tt uppercase color-1-1 fw-exlight">Localização</h1>
        <p class="pp">Acesso fácil para todas as regiões da cidade.</p>
        <p class="pp">Localizado no Grande Méier, uma região consolidada, no Vivere Residences<br>
        você contará com a infraestrutura completa de comércio, serviços, escolas e bancos e hospitais<br>
        para você e sua família.
        </p>

        <ul class="list-topics">
          <li>Shopping do Méier</li>
          <li>Rede Economia</li>
          <li>Colégio Pedro II</li>
          <li>Centro Cultural João Nogueira (Imperador)</li>
          <li>Galeria Oxford</li>
          <li>Estação do Pão</li>
          <li>Parmê</li>
          <li>Empório Gourmet</li>
          <li>Colégio Martis</li>
          <li>Colégio e Curso Interllectus</li>
          <li>Prezunic</li>
          <li>Sport Club  Mackenzie</li>
          <li>Baixo Méier</li>
          <li>Hospital Pasteur e muito mais.</li>
        </ul>
      </div>

    </div>
  </header>

  <?php include_once('inc/btn-show-mp.php'); ?>

  <main>
    <section id="page-localizacao-1" class="wrap flt-center">

    </section>
  </main>

<?php include_once('footer.php'); ?>
