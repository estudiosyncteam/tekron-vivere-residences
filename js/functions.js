
$( document ).ready( function() {

    $('.webdoor .content').slick({
  		infinite: true,
  		slidesToShow: 4,
  		slidesToScroll: 1,
  		autoplay: true,
  		autoplaySpeed: 2000,
      arrows: true,
  		responsive: [{
  			breakpoint: 900,
  			settings: {slidesToShow: 1, dots: true, arrows: false}
  		}]
  	});

  // Menu Mobile
  $('.icon-menu').click(function(){
    $('#nav-menu').slideToggle('slow');
  });

  /*
  $('.btn-menu').click(function(){
    $('#menu').slideToggle('slow');
  });*/

  // Add Active Class to Navigation Menu Item Based on URL
  function navMenuURL(){
    var path = window.location.pathname.split("/").pop();
    if(path == ""){ path = 'index.php'; }
    var target = $('#menu a[href="' + path + '"]');
    target.addClass('page-current');
  }
  navMenuURL();

  //    FORM-ATENDIMENTO
    $('.mf-popup').magnificPopup({
      type:'inline',
      midClick: true
    });


    $('#list-portfolio li span').magnificPopup({
      delegate: 'a',
      type: 'image',
      gallery: {
        enabled: true
      }
    });

    function loadingLayer(metodo,posicao,local){
			/*
			$were -> objeto onde será inserido
			Metodos -> show,hide,add,remove,hideAndRemove
			$postiions - > fixed ou absolute
			*/
			var $method = (typeof metodo === 'undefined') ? 'show' : metodo,
				$position =  (typeof posicao === 'undefined') ? 'fixed' : posicao,
				$where =  (typeof local === 'undefined') ? $('body') : local;

			if($method === 'hide'){
				$('.loader').fadeOut('fast');
			}else{
				var $loaderCreated;
				if($where.find('.loader').length === 0){
					var $loader = '<div class="loader" style="display:none;position:'+$position+'"><img src="img/loading.gif" alt="Aguarde..." /></div>';
					$loaderCreated = $($loader).appendTo($where);
				}else{
					$loaderCreated = $where.find('.loader');
				}
				$loaderCreated.fadeIn('fast');
			}
		}

		/* VALIDAÇÃO FORMS  _____________________________________________________________ */

		if(jQuery().mask){
			var SPMaskBehavior = function (val) {
				  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
				},
				spOptions = {
				  onKeyPress: function(val, e, field, options) {
					  field.mask(SPMaskBehavior.apply({}, arguments), options);
					}
				},
				optionsCpfCnpj = {onKeyPress: function(documento, e, field, optionsCpfCnpj){
					var masks = ['000.000.000-009', '00.000.000/0000-00'];
					var mask = (documento.length>14) ? masks[1] : masks[0];
					$('input[name="documento"]').mask(mask, optionsCpfCnpj);
				}};

			$('input[name="telefone"]').mask(SPMaskBehavior, spOptions);
			$('input[name="celular"]').mask(SPMaskBehavior, spOptions);
			$('input[name="cpf"]').mask('000.000.000-00');
			$('input[name="cnpj"]').mask('00.000.000/0000-00');
			$('input[name="documento"]').mask('000.000.000-00',optionsCpfCnpj);

		}

		if(jQuery().validate){
	/* CONFIGURAÇÔES VALIDATE __________________________ */

	$.validator.addMethod('fullname', function(str) {
		var match = str.match(/\b[A-Za-z]{2,}\b/g);
		return match.length > 1;
	}, 'Informe seu nome completo');

	$.validator.addMethod('cnpj', function(cnpj) {
		cnpj = jQuery.trim(cnpj);

		// DEIXA APENAS OS NÚMEROS
		 cnpj = cnpj.replace('/','');
		 cnpj = cnpj.replace('.','');
		 cnpj = cnpj.replace('.','');
		 cnpj = cnpj.replace('-','');

		var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
		if (cnpj.length == 0) {
			return false;
		}

		cnpj = cnpj.replace(/\D+/g, '');
		digitos_iguais = 1;

		for (i = 0; i < cnpj.length - 1; i++)
			if (cnpj.charAt(i) != cnpj.charAt(i + 1)) {
				digitos_iguais = 0;
				break;
			}
		if (digitos_iguais)
			return false;

		tamanho = cnpj.length - 2;
		numeros = cnpj.substring(0,tamanho);
		digitos = cnpj.substring(tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--) {
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(0)){
			return false;
		}
		tamanho = tamanho + 1;
		numeros = cnpj.substring(0,tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--) {
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2)
				pos = 9;
		}

		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

		return (resultado == digitos.charAt(1));

	}, 'Informe um CNPJ válido.');

	$.validator.addMethod("documento", function(value, element) {
		// remove pontuações
		value = value.replace('.','');
		value = value.replace('.','');
		value = value.replace('-','');
		value = value.replace('/','');

		if (value.length <= 11) {
		if(jQuery.validator.methods.cpfBR.call(this, value, element)){
			return true;
		} else {
			this.settings.messages.documento = "Informe um CPF válido.";
		}

		}
		else if (value.length <= 14) {
		if(jQuery.validator.methods.cnpj.call(this, value, element)){
			return true;
		} else {
			this.settings.messages.documento = "Informe um CNPJ válido.";
		}

		}

		return false;

	}, "Informe um documento válido.");

	$.validator.addMethod("phoneBR", function(value, element) {
		return this.optional( element ) || /(\({0,1}\d{0,2}\){0,1} {0,1})(\d{4,5}) {0,1}-{0,1}(\d{4})/.test(value);
	}, "Informe um número válido");

	$.validator.methods.email = function( value, element ) {
		return this.optional( element ) || /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test( value );
	};

	// create a custom phone number rule called 'intlTelNumber'
	// $.validator.addMethod("intlTelNumber", function(value, element) {
	//     return this.optional(element) || $(element).intlTelInput("isValidNumber");
	// }, "Insira um número válido");

	$.validator.messages.required = 'Preencha este campo';
	$.validator.messages.email = 'Digite um e-mail válido';
	$.validator.messages.cpfBR = 'Digite um CPF válido';


	$.validator.setDefaults({
		'errorElement':'span',
		'focusInvalid': true,
		'onsubmit': false,
		'rules':{
			nome_completo: {'fullname':true},
			email: {email: true},
			celular: {phoneBR:true},
			documento: {documento: true},
			cnpj: {cnpj: true},
			cpf: {cpfBR:true}
		},
		'messages':{
			telefone: {required:'Informe um número válido'},
			celular: {required:'Informe um número válido'},
			nome: {required:'Digite seu nome'},
			nome_completo: {required:'Digite seu nome completo'},
			email: {required:'Digite um e-mail válido'},
			cnpj: {required:'Digite um CNPJ válido'},
			cpf: {required:'Digite um CPF válido'},
			documento: {required:'Digite um documento válido'},
			perfil: {required:'Escolha um perfil'},
      extra: {required:'Escolha uma categoria'},
			mensagem: {required:'Preencha este campo com sua mensagem'}
		}
	});

	//Função que faz a ação de todos os formulários
	var sendForms = function($form,action,customSucess){
		var $formNode = $($form.currentForm);
		var requiredFields = $.map($form.currentElements, function(entry) {
			return entry.required === true ? entry.name : null;
		});
		var obrigatorios = JSON.stringify(requiredFields);
		$formNode.find('.return-msg').remove();
		if($form.form() === true){
			loadingLayer('show');
			var $data = $formNode.serialize()+'&obrigatorios='+obrigatorios;
			//var formData = new FormData($formNode[0]);
			//formData.append('action', action);
			//formData.append('obrigatorios',obrigatorios);
			//formData.append('pagina','home');
			$.ajax({
				type: "POST",
				url: '/sendform.php',
				data: $data,
				//data: formData,
				//processData: false,
				//contentType: false,
				timeout: 20000,
				success: function(retornoPHP) {
					var retorno = $.parseJSON(retornoPHP);
					if(retorno.sucesso){
						if (typeof callBackLandingPage === "function"){
							callBackLandingPage(4);
						}
						if (typeof customSucess === "function"){
							customSucess(retorno.sucesso);
						}else{

                if(retorno.sucesso.whatsapp){
                  window.location = 'https://wa.me/'+retorno.sucesso.whatsapp.tel+'?text='+retorno.sucesso.whatsapp.text;
                }

                if(retorno.sucesso.download){
                  //retorno.sucesso.download;
                  window.location = 'download.php';
                }

                $.magnificPopup.open({
                  items: {
                    src: '<div class="default-popup"><p class="fnt-1 fw-bold uppercase txt-center color-1-1">'+retorno.sucesso.mensagem+'</p></div>', // can be a HTML string, jQuery object, or CSS selector
                    type: 'inline'
                  },
                  fixedContentPos: false,
                  fixedBgPos: true,
                  overflowY: 'auto',
                  closeMarkup: '<button title="%title%" type="button" class="uppercase fnt-1 mfp-close">Fechar X</button>',
                  closeBtnInside: true,
                  preloader: false,
                  midClick: true,
                  removalDelay: 300,
                  mainClass: 'my-mfp-zoom-in'
                });

						}
						$formNode[0].reset();
					}else{
						if(retorno.erroEnvio){
							$.magnificPopup.open({
								items: {
									src: '<div class="default-popup"><p class="fnt-1 fw-bold uppercase txt-center color-1-1">'+retorno.erroEnvio+'</p></div>', // can be a HTML string, jQuery object, or CSS selector
									type: 'inline'
								},
								fixedContentPos: false,
								fixedBgPos: true,
								overflowY: 'auto',
								closeMarkup: '<button title="%title%" type="button" class="uppercase fnt-1 mfp-close">Fechar X</button>',
								closeBtnInside: true,
								preloader: false,
								midClick: true,
								removalDelay: 300,
								mainClass: 'my-mfp-zoom-in'
							});
						}else{
							$form.showErrors(retorno);
						}
					}

					loadingLayer('hide');

				},
				error: function(){
					$formNode.prepend('<div class="default-popup"><p class="fnt-1 fw-bold uppercase txt-center color-fff">Aconteceu um erro inesperado. Tente novamente.</p></div>');
					loadingLayer('hide');
				}
			});
		}else{
			loadingLayer('hide');
		}
	};

	/* ___________________________CONFIGURAÇÔES VALIDATE ___ */

	/* Lead cursos _____________ */

	$('.formLead').each(function () {

		var $formLead = $(this),
			widgetId = $formLead[0].formlead.value - 1,
			$lead = $formLead.validate();
			$formLead.on('submit',function(event){
				event.preventDefault(); //prevent form submit
				if (!grecaptcha.getResponse(widgetId)) {
					grecaptcha.execute(widgetId);
					loadingLayer('show');
						}else{
					$lead.form();
					loadingLayer('hide');
					if($lead.valid()){
						sendForms($lead,'sl_subscribe');
						grecaptcha.reset(widgetId);
					}
				}

			});

	});
	/* _________ Lead Cursos */

}



    //  SCROLL - BOTÃO FLUTUANTE
    $(document).scroll(function() {

        var y = $(this).scrollTop();
        if (y > 250) {
          $('.btn-show-mp').removeClass('invisible');
        } else {
          $('.btn-show-mp').addClass('invisible');
        }

    });

    $(".popup-iframe").magnificPopup({
        type: "iframe",
				midClick: true
    });

    // Portfolio - Select Dynamic
      var list_empreendimentos = {
        "Pinto de Almeida": ["Viva Pendotiba","New Soho","Eldorado I","Eldorado II","Portal de Itaipu","Portal do Sol","Portal do Verde","Viva Niterói","Blue Bay","Planet Icaraí","Quintas de Icaraí"],
        "Grupo CAP": ["Bela Vista 3","Bela Vista 4","Bela Vista 5","Solar Laranjeiras","Solar Pendotiba"],
        "União Realizações": ["Bosque do Engenho","Ferney Voltaire","Rocca Magiore","Lake View","Chambord Grimaldi","Porto Al Mare"],
        "Construtora Medeiros": ["Florence Residencial","Veneza Residencial"],
        "Fox Engenharia": ["Bela Vitta Residence","Reserva Pendotiba","Spazio Noronha"],
        "SOTER": ["Gourmet Residence","Diamond residence"],
        "Construtora Madel": ["Giardino di Capri"],
        "Scon": ["Avance"],
        "CALL": ["Villa Catalunya Residencial","Vale das Paineiras"],
        "Mascate": ["Vila da Praia","Casa Blanca","Reserva da Paz"],
        "Elvas Empreendimentos": ["Amarone","Saint Emilion","Piemonte"], // "Solar da Aldina"
        "Conipar": ["Enseada Park"],
        "Anfra": ["Tour de Saint Denis","Tour de Monaco"],
        "JPR": ["Great Place"]
      };

      jQuery.each(list_empreendimentos, function(index, valor){
        $("#select-construtoras").append('<option value="'+index+'">'+index+'</option>');
      });

      $('#select-construtoras').on('change', function(){
        var $this = $(this), $value = $this.val();
        if($value !== ''){
          var $list = list_empreendimentos[$value];
          $('#list-empreendimentos').html('');
          jQuery.each($list, function(index, valor){
            $("#list-empreendimentos").append('<li>'+valor+'</li>');
          });
        }
      });


    // Scroll entre seções/dobras de uma pagina
    $('#btn-next').on('click', function(e) {
        e.preventDefault();
        var anchor = $(this).attr('href');
        $('html,body').animate({ scrollTop: $(anchor).offset().top }, 1000);
    });

  });
