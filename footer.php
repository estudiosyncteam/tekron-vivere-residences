
  <footer id="footer" class="color-fff bg-1-1">
    <div class="wrap flt-center floatfix">

      <article id="article-form-1">
        <h1 class="tt-5 uppercase">Preencha o formulário e um de nossos consultores entrarão em contato</h1>
        <?php include_once('inc/form-leads-1.php'); ?>
      </article>

      <adress class="adress tt-5">Rua Baronesa de Uruguaiana, 84, Lins de Vasconcelos</adress>

      <div id="copyright" class="pos-relative">
        <span class="dsp-block">Copyright &copy; <?php echo date('Y'); ?> Vivere Residences. Todos os direitos reservados.</span>
        <a id="developer" href="https://www.estudiosync.com.br/" target="_blank" rel="noopener"><img src="img/estudio-sync.png" alt="Desenvolvido por estúdio Sync"></a>
      </div>
    </div>
  </footer>

  <article id="form-leads-mp2" class="white-popup mfp-hide color-fff txt-center floatfix"><div class="mfp-close">X</div>
    <h1 class="pp uppercase fw-bold">Preencha o formulário e um de nossos corretores entrarão em contato</h1>
    <?php include_once('inc/form-leads-2.php'); ?>
  </article>

  <article id="form-whatsapp" class="white-popup mfp-hide color-fff txt-center floatfix"><div class="mfp-close">X</div>
    <h1 class="pp uppercase fw-bold">Para agilizar o atendimento via whatsapp, informe os dados abaixo</h1>
    <small>*Todos os campos são de preeenchimento obrigatório.</small><br><br>
    <?php include_once('inc/form-leads-3.php'); ?>
  </article>

  <article id="form-leads-mp4" class="white-popup mfp-hide color-fff txt-center floatfix"><div class="mfp-close">X</div>
    <h1 class="pp uppercase fw-bold">Preencha o formulário e um de nossos corretores entrarão em contato</h1>
    <?php include_once('inc/form-leads-4.php'); ?>
  </article>

  <script src="js/jquery-3-3-1-min.js"></script>
  <script src="js/jquery.validate.min.js"></script>
  <script src="js/jquery.mask.min.js"></script>
  <script src="js/magnific-popup.js"></script>
  <script src="js/slick.min.js"></script>
  <script async defer src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"></script>
  <script src="js/functions.js"></script>
  <?php include_once('inc/font-load.php'); ?>
  <script src="//www.googleadservices.com/pagead/conversion_async.js"></script>
  <script>
  function callBackLandingPage(posicao){
  switch(posicao) {
      case 1:
          console.log("Nenhuma função foi definida");	break;
      case 2:
      /*
        window.google_trackConversion({
           google_conversion_id: 813153990,
           google_conversion_label: "iHhkCNfojoABEMb93oMD",
           google_remarketing_only: false,
           onload_callback : function() {
               console.log("Conversion Sent Contact");
           }
       });
       */
    }
  }
  </script>

</body>
</html>
