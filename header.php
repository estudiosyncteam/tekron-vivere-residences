<?php $server = $_SERVER['SERVER_NAME']; $endereco = $_SERVER ['REQUEST_URI']; ?>
<!DOCTYPE html>
<html lang="pt-br" class="no-js<?php echo isset($_COOKIE['fontsloaded']) ?' fonts-loaded':'' ?>">
<head>
    <meta charset="utf-8">
    <meta name="description" content="Tudo o que você precisa pagando menos, realize o sonho de ter seu primeiro imóvel com comodidade e conforto.">
    <meta name="author" content="Estúdio Sync">
    <meta name="URL" content="<?php echo $server; ?>">
    <meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Vivere Residences - Tudo o que você precisa pagando menos.</title>
    <link rel="stylesheet" href="css/magnific-popup.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="style.css">
    <link rel="icon" href="img/favicon-32x32.png">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NRC8JV8');</script>
    <!-- End Google Tag Manager -->

    <script>
	   var callBackRecaptcha1 = function(token){
			$('#formLead1').submit();
		},
		callBackRecaptcha2 = function(token){
			$('#formLead2').submit();
		},
    callBackRecaptcha3 = function(token){
			$('#formLead3').submit();
		},
    callBackRecaptcha4 = function(token){
			$('#formLead4').submit();
		},
		onloadCallback = function(){
				if($('#recaptcha1').length){
					recaptcha1 = grecaptcha.render(document.getElementById('recaptcha1'), {
						'sitekey' : '6Lf0pUkUAAAAAIFxjwesJZ_UF-Hs5P0SY4N5fm-m',
						'badge' : 'inline',
						'size' : 'invisible',
						'callback' : callBackRecaptcha1
					});
				}
				if($('#recaptcha2').length){
					recaptcha2 = grecaptcha.render(document.getElementById('recaptcha2'), {
						'sitekey' : '6Lf0pUkUAAAAAIFxjwesJZ_UF-Hs5P0SY4N5fm-m',
						'badge' : 'inline',
						'size' : 'invisible',
						'callback' : callBackRecaptcha2
					});
				}
        if($('#recaptcha3').length){
					recaptcha3 = grecaptcha.render(document.getElementById('recaptcha3'), {
						'sitekey' : '6Lf0pUkUAAAAAIFxjwesJZ_UF-Hs5P0SY4N5fm-m',
						'badge' : 'inline',
						'size' : 'invisible',
						'callback' : callBackRecaptcha3
					});
				}
        if($('#recaptcha4').length){
					recaptcha4 = grecaptcha.render(document.getElementById('recaptcha4'), {
						'sitekey' : '6Lf0pUkUAAAAAIFxjwesJZ_UF-Hs5P0SY4N5fm-m',
						'badge' : 'inline',
						'size' : 'invisible',
						'callback' : callBackRecaptcha4
					});
				}
			};
	</script>

</head>
<body class="txt-center">

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NRC8JV8"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
