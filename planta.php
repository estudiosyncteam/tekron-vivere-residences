<?php include_once('header.php'); ?>

  <header id="header" class="pos-relative page-planta">
    <div class="wrap flt-center floatfix">
      <?php include_once('inc/nav-menu.php'); ?>

      <div class="header-txt">
        <h1 class="tt uppercase color-1-1 fw-exlight">Seja bem-vindo<br>
        a uma nova forma de<br>
        viver na Zona Norte.
        </h1>
        <p class="pp">O condomínio Vivere Residences tem apartamentos de 1 a 2 quartos de 45 m² com hall privativo, apenas 4<br>
          andares e 2 blocos. Tudo isso com uma área de lazer completa, vagas de garagem, bicicletário e portaria 24 horas.<br>
          Além de ser um bairro de fácil acesso aos principais pontos da cidade.
        </p>
      </div>

    </div>
  </header>

  <?php include_once('inc/btn-show-mp.php'); ?>

  <main>
    <section id="page-planta-1">
      <div class="wrap flt-center floatfix">
        <div class="row">
          <div class="flt-left col-sm-12 col-lg-6">
            <figure class="fig-planta">
              <img src="img/planta/planta-01.jpg" alt="planta">
            </figure>
          </div>

          <div class="flt-left col-sm-12 col-lg-6">
            <div class="bloco-texto">
              <h1 class="tt-6 color-1-1 uppercase fw-exlight">Bloco 1 e 2</h1>
              <p class="tt-5 fw-medium">2º e 5º pavimento<br>
                Coluna 01 a 06<br>
                Área Privada
              </p>
            </div>

            <figure class="fig-bloco">
              <img src="img/planta/bloco-01-02.jpg" alt="planta">
            </figure>
          </div>
        </div>

        <div class="row">
          <figure class="fig-estacionamento">
            <img src="img/planta/estacionamento.jpg" alt="estacionamento">
          </figure>
        </div>

      </div>
    </section>
  </main>

<?php include_once('footer.php'); ?>
