<?php include_once('header.php'); ?>

  <header id="header" class="pos-relative page-lazer">
    <div class="wrap flt-center floatfix">
      <?php include_once('inc/nav-menu.php'); ?>

      <div class="header-txt">
        <h1 class="tt uppercase color-1-1 fw-exlight">Transforme<br>
        seus finais de semana.
        </h1>
        <p class="pp">Um espaço de lazer completo reservado para você aproveitar o ano inteiro sem<br>
          precisar sair de casa.
        </p>
      </div>

    </div>
  </header>

  <?php include_once('inc/btn-show-mp.php'); ?>

  <main>
    <section id="page-lazer-1">
      <div class="wrap flt-center">
        <ul class="list-figure color-1-1 fw-bold uppercase">
          <li>
            <figure class="fig-mask">
              <img src="img/lazer/lazer-01.jpg" alt="ambiente">
            </figure>
            <p>Piscina</p>
          </li><li>
            <figure class="fig-mask">
              <img src="img/lazer/lazer-02.jpg" alt="ambiente">
            </figure>
            <p>Espaço Gourmet</p>
          </li><li>
            <figure class="fig-mask">
              <img src="img/lazer/lazer-03.jpg" alt="ambiente">
            </figure>
            <p>Salão de Festa</p>
          </li><li>
            <figure class="fig-mask">
              <img src="img/lazer/lazer-04.jpg" alt="ambiente">
            </figure>
            <p>Churrascaria</p>
          </li><li>
            <figure class="fig-mask">
              <img src="img/lazer/lazer-05.jpg" alt="ambiente">
            </figure>
            <p>Academia</p>
          </li><li>
            <figure class="fig-mask">
              <img src="img/lazer/lazer-06.jpg" alt="ambiente">
            </figure>
            <p>Hortinha</p>
          </li>
        </ul>
      </div>
    </section>
  </main>

<?php include_once('footer.php'); ?>
